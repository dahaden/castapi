package com.dhaden.cast.events;


import android.util.Log;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.EnumSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Dave on 31/05/2015.
 */
public class EventManager <E extends Enum<E>, T> {

    private static final String TAG = "EventManager";
    private Map<String, List<ResultCallback>> _events;

    public EventManager(Class<E> className ) {

        _events = new HashMap<>();
        Iterable<E> enumSet = Collections.synchronizedSet(EnumSet.allOf(className));

        for(Enum e : enumSet) {
            _events.put(e.name(), new ArrayList<ResultCallback>());
        }
    }

    public void registerEventListener(E event, ResultCallback callback) {
        _events.get(event.name()).add(callback);
    }

    public void advertiseEvent(E event, T result) {
        if(result != null && event != null) {
            Log.d(TAG, "Event Triggered: " + event.name() + ", " + result.toString());
        }
        for(ResultCallback callback : _events.get(event.name())) {
            try {
                callback.onResult(event, result);
            } catch (Exception e) {
                Log.e(TAG, "Callback threw error", e);
            }
        }
    }
}
