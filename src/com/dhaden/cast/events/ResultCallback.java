package com.dhaden.cast.events;

/**
 * Created by Dave on 31/05/2015.
 */
public interface ResultCallback<E extends Enum, T> {

    void onResult(E event, T result);
}
