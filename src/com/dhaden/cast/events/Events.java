package com.dhaden.cast.events;

/**
 * Created by Dave on 30/05/2015.
 */
public enum Events {
    ON_ROUTE_SELECTED,
    ON_ROUTE_UNSELECTED,
    ON_CONNECTION_SUCCESS,
    ON_CONNECTION_FAILURE,
    ON_APPLICATION_STATUS_CHANGED,
    ON_APPLICATION_DISCONNECT,
    ON_VOLUME_CHANGED,
    ON_CONNECTION_SUSPENDED,
    ON_MESSAGE_RECEIVED
}
