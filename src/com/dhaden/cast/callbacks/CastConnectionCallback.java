package com.dhaden.cast.callbacks;

import android.os.Bundle;
import android.util.Log;
import com.dhaden.cast.CastRemote;
import com.dhaden.cast.channel.Channel;
import com.dhaden.cast.channel.DefaultChannel;
import com.dhaden.cast.events.Events;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.io.IOException;
import java.util.List;

/**
 * Created by Dave on 30/05/2015.
 */
public class CastConnectionCallback implements
        GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "CastConnectionCallback";
    private boolean _waitingForReconnect;
    private CastRemote _delegate;
    private String _sessionID;

    public CastConnectionCallback(CastRemote delegate) {
        _delegate = delegate;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (_waitingForReconnect) {
            reconnectChannels();
            _waitingForReconnect = false;
        } else {
            try {
                Cast.CastApi.launchApplication(_delegate.getApiClient(), _delegate.getAppID(), false)
                        .setResultCallback(getResultCallback());

            } catch (Exception e) {
                Log.e(TAG, "Failed to launch application", e);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        _waitingForReconnect = true;
        _delegate.advertiseEvent(Events.ON_CONNECTION_SUSPENDED, Integer.toString(cause));
    }

    private void reconnectChannels() {
        try {
            Cast.CastApi.joinApplication(_delegate.getApiClient(), _delegate.getAppID())
                    .setResultCallback(getResultCallback());
        } catch (Exception e) {
            Log.e(TAG, "Failed to launch application", e);
            _delegate.advertiseEvent(Events.ON_APPLICATION_DISCONNECT, "1");
        }
    }

    public void connectChannels() {
        List<Channel> channels = _delegate.getChannels();
        for (Channel channel : channels) {
            connectChannel(channel);
        }
    }

    public void connectChannel(Channel channel) {
        try {
            Cast.CastApi.setMessageReceivedCallbacks(_delegate.getApiClient(),
                    channel.getNamespace(),
                    channel);
        } catch (IOException e) {
            Log.e(TAG, "Exception while creating channel: " + channel.getNamespace(), e);
        }
    }

    private ResultCallback<Cast.ApplicationConnectionResult> getResultCallback() {
        return new ResultCallback<Cast.ApplicationConnectionResult>() {
            @Override
            public void onResult(Cast.ApplicationConnectionResult result) {
                Status status = result.getStatus();
                if (status.isSuccess()) {
                    ApplicationMetadata applicationMetadata =
                            result.getApplicationMetadata();
                    _sessionID = result.getSessionId();
                    String applicationStatus = result.getApplicationStatus();
                    boolean wasLaunched = result.getWasLaunched();
                    connectChannels();
                    _delegate.advertiseEvent(Events.ON_CONNECTION_SUCCESS, _sessionID);
                } else {
                    _delegate.advertiseEvent(Events.ON_CONNECTION_FAILURE, status.getStatusMessage());
                }
            }
        };
    }

    public void sendMessage(String message, Channel channel) {
        if (_delegate.getApiClient() != null && channel != null) {
            try {
                Cast.CastApi.sendMessage(_delegate.getApiClient(), channel.getNamespace(), message)
                        .setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status result) {
                                        if (!result.isSuccess()) {
                                            Log.e(TAG, "Sending message failed");
                                        }
                                    }
                                });
            } catch (Exception e) {
                Log.e(TAG, "Exception while sending message", e);
            }
        }
    }

    public void messageReceived(String message, DefaultChannel defaultChannel) {
        _delegate.advertiseEvent(Events.ON_MESSAGE_RECEIVED, defaultChannel.getNamespace() + "~" + message);
    }

    public boolean isSuspended() {
        return _waitingForReconnect;
    }

    public String getSessionID() {
        return _sessionID;
    }
}
