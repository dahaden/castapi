package com.dhaden.cast.listener;

import android.util.Log;
import com.dhaden.cast.CastRemote;
import com.dhaden.cast.events.Events;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by Dave on 30/05/2015.
 */
public class SuccessListener extends Cast.Listener {

    private static final String TAG = "SuccessListener";
    private CastRemote _delegate;

    public SuccessListener(CastRemote delegate) {
        _delegate = delegate;
    }

    @Override
    public void onApplicationStatusChanged() {
        GoogleApiClient apiClient = _delegate.getApiClient();
        if (apiClient != null) {
            Log.d(TAG, "onApplicationStatusChanged: "
                    + Cast.CastApi.getApplicationStatus(apiClient));
            _delegate.advertiseEvent(Events.ON_APPLICATION_STATUS_CHANGED, Cast.CastApi.getApplicationStatus(apiClient));
        }
    }

    @Override
    public void onVolumeChanged() {
        GoogleApiClient apiClient = _delegate.getApiClient();
        if (apiClient != null) {
            Log.d(TAG, "onVolumeChanged: " + Cast.CastApi.getVolume(apiClient));
            _delegate.advertiseEvent(Events.ON_VOLUME_CHANGED, Double.toString(Cast.CastApi.getVolume(apiClient)));
        }
    }

    @Override
    public void onApplicationDisconnected(int errorCode) {
        Log.d(TAG, "onApplicationDisconnected: " + errorCode);
        _delegate.advertiseEvent(Events.ON_APPLICATION_STATUS_CHANGED, Integer.toString(errorCode));
    }
}
