package com.dhaden.cast.listener;

import android.util.Log;
import com.dhaden.cast.CastRemote;
import com.dhaden.cast.events.Events;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by Dave on 30/05/2015.
 */
public class FailedListener implements
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "FailedListener";
    private CastRemote _delegate;

    public FailedListener(CastRemote delegate) {
        _delegate = delegate;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "Failed To Connect: " + connectionResult.describeContents() + ", " + connectionResult.getErrorCode());
        _delegate.advertiseEvent(Events.ON_CONNECTION_FAILURE, Integer.toString(connectionResult.getErrorCode()));
    }
}
