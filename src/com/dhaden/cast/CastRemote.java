package com.dhaden.cast;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.MediaRouteActionProvider;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.view.Menu;
import android.view.MenuItem;
import com.dhaden.cast.callbacks.CastConnectionCallback;
import com.dhaden.cast.channel.Channel;
import com.dhaden.cast.channel.DefaultChannel;
import com.dhaden.cast.events.EventManager;
import com.dhaden.cast.events.Events;
import com.dhaden.cast.events.ResultCallback;
import com.dhaden.cast.listener.FailedListener;
import com.dhaden.cast.listener.SuccessListener;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dave on 30/05/2015.
 */
public abstract class CastRemote extends AppCompatActivity {
    private MediaRouter _mediaRouter;
    private MediaRouteSelector _mediaRouteSelector;
    private CastMediaRouterCallback _castMediaRouterCallback;
    private GoogleApiClient _apiClient;
    private CastConnectionCallback _castConnectionCallbacks;

    private List<Channel> _channels;
    private List<String> _namespaces;

    private EventManager<Events, String> _eventManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _mediaRouter = MediaRouter.getInstance(getApplicationContext());

        _mediaRouteSelector = new MediaRouteSelector.Builder()
                .addControlCategory(CastMediaControlIntent.categoryForCast(getAppID()))
                .build();
        _castMediaRouterCallback = new CastMediaRouterCallback(this);

        _channels = new ArrayList<>();
        _namespaces = new ArrayList<>();
        _eventManager = new EventManager<>(Events.class);
        _eventManager.registerEventListener(Events.ON_APPLICATION_DISCONNECT, getTearDownCallback());
        _eventManager.registerEventListener(Events.ON_CONNECTION_FAILURE, getTearDownCallback());
        _eventManager.registerEventListener(Events.ON_ROUTE_UNSELECTED, getTearDownCallback());

        _eventManager.registerEventListener(Events.ON_ROUTE_SELECTED, new ResultCallback<Events, String>() {
            @Override
            public void onResult(Events event, String result) {
                routeSelected();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem mediaRouteMenuItem = menu.findItem(R.id.media_route_menu_item);
        MediaRouteActionProvider mediaRouteActionProvider =
                (MediaRouteActionProvider) MenuItemCompat.getActionProvider(mediaRouteMenuItem);
        mediaRouteActionProvider.setRouteSelector(_mediaRouteSelector);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        _mediaRouter.addCallback(_mediaRouteSelector, _castMediaRouterCallback,
                MediaRouter.CALLBACK_FLAG_REQUEST_DISCOVERY);
    }

    @Override
    protected void onPause() {
        if (isFinishing()) {
            _mediaRouter.removeCallback(_castMediaRouterCallback);
        }
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        _mediaRouter.addCallback(_mediaRouteSelector, _castMediaRouterCallback,
                MediaRouter.CALLBACK_FLAG_REQUEST_DISCOVERY);
    }

    @Override
    protected void onStop() {
        _mediaRouter.removeCallback(_castMediaRouterCallback);
        super.onStop();
    }

    public void teardown() {
        _castConnectionCallbacks = null;
        _apiClient = null;
    }

    public abstract String getAppID();

    public void routeSelected() {
        Cast.CastOptions.Builder apiOptionsBuilder = Cast.CastOptions
                .builder(_castMediaRouterCallback.getSelectedDevice(), new SuccessListener(this));

        _castConnectionCallbacks = new CastConnectionCallback(this);

        _apiClient = new GoogleApiClient.Builder(this)
                .addApi(Cast.API, apiOptionsBuilder.build())
                .addConnectionCallbacks(_castConnectionCallbacks)
                .addOnConnectionFailedListener(new FailedListener(this))
                .build();
        _apiClient.connect();
    }

    public GoogleApiClient getApiClient() {
        return _apiClient;
    }

    public List<Channel> getChannels() {
        return _channels;
    }

    public void addChannel(Channel channel) {
        _channels.add(channel);
    }

    public void addChannels(List<Channel> channels) {
        _channels.addAll(channels);
    }

    public void sendMessage(String message, Channel channel) {
        _castConnectionCallbacks.sendMessage(message, channel);
    }

    public String getSessionID() {
        if(_castConnectionCallbacks != null) {
            return _castConnectionCallbacks.getSessionID();
        }
        return null;
    }

    public void registerEvent(Events event, ResultCallback<Events, String> callback) {
        _eventManager.registerEventListener(event, callback);
    }

    public void advertiseEvent(Events event, String result) {
        _eventManager.advertiseEvent(event, result);
    }

    private ResultCallback<Events, String> getTearDownCallback() {
        return new ResultCallback<Events, String>() {
            @Override
            public void onResult(Events event, String result) {
                teardown();
            }
        };
    }
}
