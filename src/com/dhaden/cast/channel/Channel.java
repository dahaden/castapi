package com.dhaden.cast.channel;

import com.google.android.gms.cast.Cast;

/**
 * Created by Dave on 30/05/2015.
 */
public interface Channel extends Cast.MessageReceivedCallback {
    String getNamespace();
}
