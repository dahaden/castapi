package com.dhaden.cast.channel;

import android.util.Log;
import com.dhaden.cast.callbacks.CastConnectionCallback;
import com.google.android.gms.cast.CastDevice;

/**
 * Created by Dave on 30/05/2015.
 */
public class DefaultChannel implements Channel {

    private static final String TAG = "DefaultChannel";
    private String _namespace;
    private CastConnectionCallback _delegate;

    public DefaultChannel(CastConnectionCallback delegate, String namespace) {
        _namespace = namespace;
        _delegate = delegate;
    }

    @Override
    public String getNamespace() {
        return _namespace;
    }

    @Override
    public void onMessageReceived(CastDevice castDevice, String namespace, String message) {
        Log.d(TAG, "onMessageReceived: " + message);
        _delegate.messageReceived(message, this);
    }

    public void reply(String message) {
        _delegate.sendMessage(message, this);
    }
}
