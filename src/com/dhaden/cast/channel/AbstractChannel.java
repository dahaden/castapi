package com.dhaden.cast.channel;

/**
 * Created by Dave on 30/05/2015.
 */
public abstract class AbstractChannel implements Channel {

    private String _namespace;

    public AbstractChannel(String namespace) {
        _namespace = namespace;
    }

    public String getNamespace() {
        return _namespace;
    }
}
