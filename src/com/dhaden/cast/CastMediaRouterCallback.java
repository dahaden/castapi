package com.dhaden.cast;

import android.support.v7.media.MediaRouter;
import com.dhaden.cast.events.Events;
import com.google.android.gms.cast.CastDevice;

/**
 * Created by Dave on 30/05/2015.
 */
public class CastMediaRouterCallback extends MediaRouter.Callback {

    private CastDevice _selectedDevice;
    private String _routeId;
    private CastRemote _delegate;

    public CastMediaRouterCallback(CastRemote delegate) {
        _delegate = delegate;
    }

    @Override
    public void onRouteSelected(MediaRouter router, MediaRouter.RouteInfo info) {
        _selectedDevice = CastDevice.getFromBundle(info.getExtras());
        _routeId = info.getId();
        _delegate.advertiseEvent(Events.ON_ROUTE_SELECTED, info.getId());
    }

    @Override
    public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo info) {
        _delegate.advertiseEvent(Events.ON_ROUTE_UNSELECTED, info.getId());
        _selectedDevice = null;
    }

    public CastDevice getSelectedDevice() {
        return _selectedDevice;
    }

    public String getRouteId() {
        return _routeId;
    }
}